package model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Wellington on 08/10/2014.
 */
public class DataBaseConnection extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "oquecomprar";
    private static final int DATABASE_VERSION = 1;

    public DataBaseConnection(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE DATABASE IF NOT EXISTS itemcompra (" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                " nome VARVHAR(200) NOT NULL, " +
                " quantidade INTEGER NOT NULL DEFAULT 1, " +
                "observacao TEXT" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

package model;

/**
 * Created by Wellington on 08/10/2014.
 */
public class ItemCompra {

    private int id;
    private  String nome;
    private int quantidade;
    private String observacao;

    public ItemCompra(int id, String nome, int quantidade, String observacao) {
        this.id = id;
        this.nome = nome;
        this.quantidade = quantidade;
        this.observacao = observacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}

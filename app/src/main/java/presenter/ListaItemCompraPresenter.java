package presenter;

import java.util.ArrayList;

import model.DataBaseConnection;
import model.ItemCompra;
import model.ItemCompraDAO;
import view.ListaItemCompraView;

/**
 * Created by Wellington on 08/10/2014.
 */
public class ListaItemCompraPresenter {

    private ListaItemCompraView view;
    private ItemCompraDAO itemCompraDAO;

    public ListaItemCompraPresenter(ListaItemCompraView view, ItemCompraDAO itemCompraDAO){
        this.view = view;
        this.itemCompraDAO = itemCompraDAO;
    }

    public void init(){

        ArrayList<ItemCompra> itensCompra = new ArrayList<ItemCompra>();
        itensCompra.add(new ItemCompra(1, "Item 1", 1, "Observação"));
        itensCompra.add(new ItemCompra(2, "Item 2", 2, "Observação"));
        itensCompra.add(new ItemCompra(3, "Item 3", 3, "Observação"));
        itensCompra.add(new ItemCompra(4, "Item 4", 4, "Observação"));
        view.listarItensCompra(itensCompra);
    }

}

package com.wellingtonwa.oquecomprar;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import model.DataBaseConnection;
import model.ItemCompra;
import model.ItemCompraDAO;
import presenter.ListaItemCompraPresenter;
import view.ListViewItemCompraAdapter;
import view.ListaItemCompraView;


public class ListaItemCompraActivity extends Activity implements ListaItemCompraView {

    private ListaItemCompraPresenter presenter;
    private ListView listViewItemCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_item_compra);
        listViewItemCompra = (ListView) findViewById(R.id.listView1);
        DataBaseConnection dataBaseConnection = new DataBaseConnection(this);
        ItemCompraDAO itemCompraDAO = new ItemCompraDAO(dataBaseConnection.getWritableDatabase());
        presenter = new ListaItemCompraPresenter(this, itemCompraDAO);
        presenter.init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lista_item_compra, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void listarItensCompra(List<ItemCompra> itemCompraList) {
        listViewItemCompra.setAdapter(new ListViewItemCompraAdapter(itemCompraList, this));
    }
}

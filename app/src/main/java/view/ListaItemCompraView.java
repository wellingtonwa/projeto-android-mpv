package view;

import java.util.List;

import model.ItemCompra;

/**
 * Created by Wellington on 08/10/2014.
 */
public interface ListaItemCompraView {

    public void listarItensCompra(List<ItemCompra> itemCompraList);

}

package view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wellingtonwa.oquecomprar.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import model.ItemCompra;

/**
 * Created by Wellington on 08/10/2014.
 */
public class ListViewItemCompraAdapter extends BaseAdapter {

    private List<ItemCompra> listaItemCompra;
    private Context context;

    public ListViewItemCompraAdapter(List<ItemCompra> listaItemCompra, Context context) {
        this.listaItemCompra = listaItemCompra;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listaItemCompra.size();
    }

    @Override
    public Object getItem(int position) {
        return listaItemCompra.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaItemCompra.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.listviewitem_itemcompra, null);
        ItemCompra itemCompra = (ItemCompra) getItem(position);
        TextView nome = (TextView) view.findViewById(R.id.textViewNome);
        TextView quantidade = (TextView) view.findViewById(R.id.textViewQuantidade);
        nome.setText(itemCompra.getNome());
        quantidade.setText(String.valueOf(itemCompra.getQuantidade()));
        return view;
    }
}
